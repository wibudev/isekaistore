<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-main',
    'version' => 'dev-main',
    'aliases' => 
    array (
    ),
    'reference' => '489244cc312dca244bb5f3e6d19ec6d6721a13ac',
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
      ),
      'reference' => '489244cc312dca244bb5f3e6d19ec6d6721a13ac',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '2.0.5',
      'version' => '2.0.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ade2b3bbfb776f27f0558e26eed43b5d9fe1b392',
    ),
    'doctrine/lexer' => 
    array (
      'pretty_version' => '1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c268e882d4dbdd85e36e4ad69e02dc284f89d229',
    ),
    'egulias/email-validator' => 
    array (
      'pretty_version' => '3.2.1',
      'version' => '3.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f88dcf4b14af14a98ad96b14b2b317969eab6715',
    ),
    'illuminate/bus' => 
    array (
      'pretty_version' => 'v9.31.0',
      'version' => '9.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'baccdba32ec4e7d3492cfd991806a8ead397f77f',
    ),
    'illuminate/collections' => 
    array (
      'pretty_version' => 'v9.31.0',
      'version' => '9.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a533ba3fce4e288a42f6287e1b9a685cdbc14f9f',
    ),
    'illuminate/conditionable' => 
    array (
      'pretty_version' => 'v9.31.0',
      'version' => '9.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5b40f51ccb07e0e7b1ec5559d8db9e0e2dc51883',
    ),
    'illuminate/console' => 
    array (
      'pretty_version' => 'v9.31.0',
      'version' => '9.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0b627ee1deb9f9d99ea116918ddad2f694bc038e',
    ),
    'illuminate/container' => 
    array (
      'pretty_version' => 'v9.31.0',
      'version' => '9.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8ca3036459e26dc7cdedaf0f882b625757cc341e',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => 'v9.31.0',
      'version' => '9.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd57130115694b4f6a98d064bea31cdb09d7784f8',
    ),
    'illuminate/events' => 
    array (
      'pretty_version' => 'v9.31.0',
      'version' => '9.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '8e534676bac23bc17925f5c74c128f9c09b98f69',
    ),
    'illuminate/filesystem' => 
    array (
      'pretty_version' => 'v9.31.0',
      'version' => '9.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2579700c62f5eb767305759d5b31da6697462096',
    ),
    'illuminate/macroable' => 
    array (
      'pretty_version' => 'v9.31.0',
      'version' => '9.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e3bfaf6401742a9c6abca61b9b10e998e5b6449a',
    ),
    'illuminate/pipeline' => 
    array (
      'pretty_version' => 'v9.31.0',
      'version' => '9.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e0be3f3f79f8235ad7334919ca4094d5074e02f6',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => 'v9.31.0',
      'version' => '9.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '720d5ce69156a3d76ee42b79ca5636f3b5010e15',
    ),
    'illuminate/translation' => 
    array (
      'pretty_version' => 'v9.31.0',
      'version' => '9.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a07a9d917f94b9a5bced15a24e1e995c5e923db',
    ),
    'illuminate/validation' => 
    array (
      'pretty_version' => 'v9.31.0',
      'version' => '9.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '96f261db2a53689d2538cf732b8f6b27dcba4b9f',
    ),
    'illuminate/view' => 
    array (
      'pretty_version' => 'v9.31.0',
      'version' => '9.31.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '913c3c5ae2228525d08432c5f91f5f6f53da68fb',
    ),
    'laravel/breeze' => 
    array (
      'pretty_version' => 'v1.13.1',
      'version' => '1.13.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1c7592561f5a834eea6c38a77e43ae3c2ee83609',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.62.1',
      'version' => '2.62.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '01bc4cdefe98ef58d1f9cb31bdbbddddf2a88f7a',
    ),
    'nunomaduro/termwind' => 
    array (
      'pretty_version' => 'v1.14.0',
      'version' => '1.14.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '10065367baccf13b6e30f5e9246fa4f63a79eb1d',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c71ecc56dfe541dbd90c5360474fbc405f8d5963',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.1|2.0',
      ),
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0|2.0|3.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '764e0b3939f5ca87cb904f570ef9be2d78a07865',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v6.0.12',
      'version' => '6.0.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c5c2e313aa682530167c25077d6bdff36346251e',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '26954b3d62a6c5fd0ea8a2a00c0353a14978d05c',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v6.0.11',
      'version' => '6.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '09cb683ba5720385ea6966e5e06be2a34f2568b1',
    ),
    'symfony/http-foundation' => 
    array (
      'pretty_version' => 'v6.0.12',
      'version' => '6.0.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd50ee4795c981638369dfa0b281107365fab2429',
    ),
    'symfony/mime' => 
    array (
      'pretty_version' => 'v6.0.12',
      'version' => '6.0.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '02a11577f2f9522c783179712bdf6d2d3cb9fc1d',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6fd1b9a79f6e3cf65f9e679b23af304cd9e010d4',
    ),
    'symfony/polyfill-intl-grapheme' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '433d05519ce6990bf3530fba6957499d327395c2',
    ),
    'symfony/polyfill-intl-idn' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '59a8d271f00dd0e4c2e518104cc7963f655a1aa8',
    ),
    'symfony/polyfill-intl-normalizer' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '219aa369ceff116e673852dce47c3a41794c14bd',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '9344f9cb97f3b19424af1a21a3b0e75b0a7d8d7e',
    ),
    'symfony/polyfill-php72' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bf44a9fd41feaac72b074de600314a93e2ae78e2',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.26.0',
      'version' => '1.26.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cfa0ae98841b9e461207c13ab093d76b0fa7bace',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v6.0.11',
      'version' => '6.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '44270a08ccb664143dede554ff1c00aaa2247a43',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd78d39c1599bd1188b8e26bb341da52c3c6d8a66',
    ),
    'symfony/string' => 
    array (
      'pretty_version' => 'v6.0.12',
      'version' => '6.0.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '3a975ba1a1508ad97df45f4590f55b7cc4c1a0a0',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v6.0.12',
      'version' => '6.0.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e71973b4991e141271465dacf4bf9e719941424',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v3.0.2',
      'version' => '3.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'acbfbb274e730e5a0236f619b6168d9dedb3e282',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '2.3|3.0',
      ),
    ),
    'voku/portable-ascii' => 
    array (
      'pretty_version' => '2.0.1',
      'version' => '2.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b56450eed252f6801410d810c8e1727224ae0743',
    ),
  ),
);
