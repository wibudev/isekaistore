# IsekaiStore

IsekaiStore is a web marketplace that sells weeb stuff, like anime figures, manga, and other stuff. Build by WibuDev Team.

How to initial this project on your computer :

- Clone this project
- Open terminal at IsekaiProject folder
- Run 'composer install'
- cd to 'breeze-next' folder
- Run 'npm install'
- Run 'npm run dev' to start the frontend-project
- cd back to IsekaiProject folder
- Run 'php artisan serve' to start the backend-project
